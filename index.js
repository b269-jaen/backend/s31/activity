// Create a server with routes

let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if( request.url == '/login'){
		response.writeHead(200, {'Context-type': 'text/plain'});
		response.end('Welcome to the login page.')
	}else if ( request.url == '/register'){
		response.writeHead(200, {'Context-type': 'text/plain'});
		response.end(`I'm sorry the page you are looking for cannot be found.`)
	}else {
		response.writeHead(200, {'Context-type': 'text/plain'});
		response.end('Page not available! try adding url routes')
	}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);